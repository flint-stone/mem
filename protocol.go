package mem

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/util/ca"
)

var accessReqByteOverhead = 12
var accessRspByteOverhead = 4

// AccessReq abstracts read and write requests that are sent to the
// cache modules or memory controllers.
type AccessReq interface {
	akita.Msg
	GetAddress() uint64
	GetByteSize() uint64
	GetPID() ca.PID
}

// A AccessRsp is a respond in the memory system.
type AccessRsp interface {
	akita.Msg
	GetRespondTo() string
}

// A ReadReq is a request sent to a memory controller to fetch data
type ReadReq struct {
	akita.MsgMeta

	Address            uint64
	AccessByteSize     uint64
	PID                ca.PID
	PC                 uint64
	CanWaitForCoalesce bool
}

// Meta returns the message meta.
func (r *ReadReq) Meta() *akita.MsgMeta {
	return &r.MsgMeta
}

// GetByteSize returns the number of byte that the request is accessing.
func (r *ReadReq) GetByteSize() uint64 {
	return r.AccessByteSize
}

// GetAddress returns the address that the request is accessing
func (r *ReadReq) GetAddress() uint64 {
	return r.Address
}

// GetPID returns the process ID that the request is working on.
func (r *ReadReq) GetPID() ca.PID {
	return r.PID
}

// ReadReqBuilder can build read requests.
type ReadReqBuilder struct {
	sendTime           akita.VTimeInSec
	src, dst           akita.Port
	pid                ca.PID
	pc                 uint64
	address, byteSize  uint64
	canWaitForCoalesce bool
}

// WithSendTime sets the send time of the request to build.
func (b ReadReqBuilder) WithSendTime(t akita.VTimeInSec) ReadReqBuilder {
	b.sendTime = t
	return b
}

// WithSrc sets the source of the request to build.
func (b ReadReqBuilder) WithSrc(src akita.Port) ReadReqBuilder {
	b.src = src
	return b
}

// WithDst sets the destination of the request to build.
func (b ReadReqBuilder) WithDst(dst akita.Port) ReadReqBuilder {
	b.dst = dst
	return b
}

// WithPID sets the PID of the request to build.
func (b ReadReqBuilder) WithPID(pid ca.PID) ReadReqBuilder {
	b.pid = pid
	return b
}

// WithPC sets the PC of the request to build.
func (b ReadReqBuilder) WithPC(pc uint64) ReadReqBuilder {
	b.pc = pc
	return b
}

// WithAddress sets the address of the request to build.
func (b ReadReqBuilder) WithAddress(address uint64) ReadReqBuilder {
	b.address = address
	return b
}

// WithByteSize sets the byte size of the request to build.
func (b ReadReqBuilder) WithByteSize(byteSize uint64) ReadReqBuilder {
	b.byteSize = byteSize
	return b
}

// CanWaitForCoalesce allow the request to build to wait for coalesce.
func (b ReadReqBuilder) CanWaitForCoalesce() ReadReqBuilder {
	b.canWaitForCoalesce = true
	return b
}

// Build creates a new ReadReq
func (b ReadReqBuilder) Build() *ReadReq {
	r := &ReadReq{}
	r.ID = akita.GetIDGenerator().Generate()
	r.Src = b.src
	r.Dst = b.dst
	r.SendTime = b.sendTime
	r.TrafficBytes = accessReqByteOverhead
	r.Address = b.address
	r.PID = b.pid
	r.PC = b.pc
	r.AccessByteSize = b.byteSize
	r.CanWaitForCoalesce = b.canWaitForCoalesce
	return r
}

// A WriteReq is a request sent to a memory controller to write data
type WriteReq struct {
	akita.MsgMeta

	Address            uint64
	Data               []byte
	DirtyMask          []bool
	PID                ca.PID
	PC                 uint64
	CanWaitForCoalesce bool
}

// Meta returns the meta data attached to a request.
func (r *WriteReq) Meta() *akita.MsgMeta {
	return &r.MsgMeta
}

// GetByteSize returns the number of byte that the request is writing.
func (r *WriteReq) GetByteSize() uint64 {
	return uint64(len(r.Data))
}

// GetAddress returns the address that the request is accessing
func (r *WriteReq) GetAddress() uint64 {
	return r.Address
}

// GetPID returns the PID of the read address
func (r *WriteReq) GetPID() ca.PID {
	return r.PID
}

// WriteReqBuilder can build read requests.
type WriteReqBuilder struct {
	sendTime           akita.VTimeInSec
	src, dst           akita.Port
	pid                ca.PID
	pc                 uint64
	address            uint64
	data               []byte
	dirtyMask          []bool
	canWaitForCoalesce bool
}

// WithSendTime sets the send time of the message to build.
func (b WriteReqBuilder) WithSendTime(t akita.VTimeInSec) WriteReqBuilder {
	b.sendTime = t
	return b
}

// WithSrc sets the source of the request to build.
func (b WriteReqBuilder) WithSrc(src akita.Port) WriteReqBuilder {
	b.src = src
	return b
}

// WithDst sets the destination of the request to build.
func (b WriteReqBuilder) WithDst(dst akita.Port) WriteReqBuilder {
	b.dst = dst
	return b
}

// WithPID sets the PID of the request to build.
func (b WriteReqBuilder) WithPID(pid ca.PID) WriteReqBuilder {
	b.pid = pid
	return b
}

// WithPC sets the pc of the request to build.
func (b WriteReqBuilder) WithPC(pc uint64) WriteReqBuilder {
	b.pc = pc
	return b
}

// WithAddress sets the address of the request to build.
func (b WriteReqBuilder) WithAddress(address uint64) WriteReqBuilder {
	b.address = address
	return b
}

// WithData sets the data of the request to build.
func (b WriteReqBuilder) WithData(data []byte) WriteReqBuilder {
	b.data = data
	return b
}

// WithDirtyMask sets the dirty mask of the request to build.
func (b WriteReqBuilder) WithDirtyMask(mask []bool) WriteReqBuilder {
	b.dirtyMask = mask
	return b
}

// CanWaitForCoalesce allow the request to build to wait for coalesce.
func (b WriteReqBuilder) CanWaitForCoalesce() WriteReqBuilder {
	b.canWaitForCoalesce = true
	return b
}

// Build creates a new WriteReq
func (b WriteReqBuilder) Build() *WriteReq {
	r := &WriteReq{}
	r.ID = akita.GetIDGenerator().Generate()
	r.Src = b.src
	r.Dst = b.dst
	r.SendTime = b.sendTime
	r.PID = b.pid
	r.PC = b.pc
	r.Address = b.address
	r.Data = b.data
	r.TrafficBytes = len(r.Data) + accessReqByteOverhead
	r.DirtyMask = b.dirtyMask
	r.CanWaitForCoalesce = b.canWaitForCoalesce
	return r
}

// A DataReadyRsp is the respond sent from the lower module to the higher
// module that carries the data loaded.
type DataReadyRsp struct {
	akita.MsgMeta

	RespondTo string // The ID of the request it replies
	Data      []byte
}

// Meta returns the meta data attached to each message.
func (r *DataReadyRsp) Meta() *akita.MsgMeta {
	return &r.MsgMeta
}

// GetRespondTo returns the ID if the request that the respond is resonding to.
func (r *DataReadyRsp) GetRespondTo() string {
	return r.RespondTo
}

// DataReadyRspBuilder can build data ready responds.
type DataReadyRspBuilder struct {
	sendTime akita.VTimeInSec
	src, dst akita.Port
	rspTo    string
	data     []byte
}

// WithSendTime sets the send time of the request to build.
func (b DataReadyRspBuilder) WithSendTime(
	t akita.VTimeInSec,
) DataReadyRspBuilder {
	b.sendTime = t
	return b
}

// WithSrc sets the source of the request to build.
func (b DataReadyRspBuilder) WithSrc(src akita.Port) DataReadyRspBuilder {
	b.src = src
	return b
}

// WithDst sets the destination of the request to build.
func (b DataReadyRspBuilder) WithDst(dst akita.Port) DataReadyRspBuilder {
	b.dst = dst
	return b
}

// WithRspTo sets ID of the request that the respond to build is replying to.
func (b DataReadyRspBuilder) WithRspTo(id string) DataReadyRspBuilder {
	b.rspTo = id
	return b
}

// WithData sets the data of the request to build.
func (b DataReadyRspBuilder) WithData(data []byte) DataReadyRspBuilder {
	b.data = data
	return b
}

// Build creates a new DataReadyRsp
func (b DataReadyRspBuilder) Build() *DataReadyRsp {
	r := &DataReadyRsp{}
	r.ID = akita.GetIDGenerator().Generate()
	r.Src = b.src
	r.Dst = b.dst
	r.SendTime = b.sendTime
	r.TrafficBytes = len(b.data) + accessRspByteOverhead
	r.RespondTo = b.rspTo
	r.Data = b.data
	return r
}

// A WriteDoneRsp is a respond sent from the lower module to the higher module
// to mark a previous requests is completed successfully.
type WriteDoneRsp struct {
	akita.MsgMeta

	RespondTo string
}

// Meta returns the meta data accociated with the message.
func (r *WriteDoneRsp) Meta() *akita.MsgMeta {
	return &r.MsgMeta
}

// GetRespondTo returns the ID of the request that the respond is responding to.
func (r *WriteDoneRsp) GetRespondTo() string {
	return r.RespondTo
}

// WriteDoneRspBuilder can build data ready responds.
type WriteDoneRspBuilder struct {
	sendTime akita.VTimeInSec
	src, dst akita.Port
	rspTo    string
}

// WithSendTime sets the send time of the message to build.
func (b WriteDoneRspBuilder) WithSendTime(
	t akita.VTimeInSec,
) WriteDoneRspBuilder {
	b.sendTime = t
	return b
}

// WithSrc sets the source of the request to build.
func (b WriteDoneRspBuilder) WithSrc(src akita.Port) WriteDoneRspBuilder {
	b.src = src
	return b
}

// WithDst sets the destination of the request to build.
func (b WriteDoneRspBuilder) WithDst(dst akita.Port) WriteDoneRspBuilder {
	b.dst = dst
	return b
}

// WithRspTo sets ID of the request that the respond to build is replying to.
func (b WriteDoneRspBuilder) WithRspTo(id string) WriteDoneRspBuilder {
	b.rspTo = id
	return b
}

// Build creates a new WriteDoneRsp
func (b WriteDoneRspBuilder) Build() *WriteDoneRsp {
	r := &WriteDoneRsp{}
	r.ID = akita.GetIDGenerator().Generate()
	r.Src = b.src
	r.Dst = b.dst
	r.TrafficBytes = accessRspByteOverhead
	r.SendTime = b.sendTime
	r.RespondTo = b.rspTo
	return r
}
