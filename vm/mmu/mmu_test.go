package mmu

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem/vm"
	"gitlab.com/akita/util/ca"
)

var _ = Describe("MMU", func() {

	var (
		mockCtrl      *gomock.Controller
		engine        *MockEngine
		toTop         *MockPort
		migrationPort *MockPort
		topSender     *MockBufferedSender
		pageTable     *MockPageTable
		mmu           *MMUImpl
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		engine = NewMockEngine(mockCtrl)
		toTop = NewMockPort(mockCtrl)
		migrationPort = NewMockPort(mockCtrl)
		topSender = NewMockBufferedSender(mockCtrl)
		pageTable = NewMockPageTable(mockCtrl)

		builder := MakeBuilder().WithEngine(engine)
		mmu = builder.Build("mmu")
		mmu.ToTop = toTop
		mmu.topSender = topSender
		mmu.MigrationPort = migrationPort
		mmu.pageTable = pageTable
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	Context("parse top", func() {
		It("should process translation request", func() {
			translationReq := vm.TranslationReqBuilder{}.
				WithSendTime(10).
				WithDst(mmu.ToTop).
				WithPID(1).
				WithVAddr(0x100000100).
				WithGPUID(0).
				Build()
			toTop.EXPECT().
				Retrieve(akita.VTimeInSec(10)).
				Return(translationReq)

			mmu.parseFromTop(10)

			Expect(mmu.numRequestsInFlight == 1)
		})

		It("should set MMU to max in flight requests", func() {
			mmu.numRequestsInFlight = 15
			translationReq := vm.TranslationReqBuilder{}.
				WithSendTime(10).
				WithDst(mmu.ToTop).
				WithPID(1).
				WithVAddr(0x100000100).
				WithGPUID(0).
				Build()
			toTop.EXPECT().
				Retrieve(akita.VTimeInSec(10)).
				Return(translationReq)

			mmu.parseFromTop(10)

			Expect(mmu.numRequestsInFlight == 16)
		})

		It("should stall parse from top if MMU is servicing max requests", func() {
			mmu.numRequestsInFlight = 16

			madeProgress := mmu.parseFromTop(10)

			Expect(madeProgress).To(BeFalse())
		})
	})

	Context("walk page table", func() {
		It("should reduce translation cycles", func() {
			req := vm.TranslationReqBuilder{}.
				WithSendTime(10).
				WithDst(toTop).
				WithPID(1).
				WithVAddr(0x1020).
				WithGPUID(0).
				Build()
			walking := transaction{req: req, cycleLeft: 10}
			mmu.walkingTranslations = append(mmu.walkingTranslations, walking)

			madeProgress := mmu.walkPageTable(11)

			Expect(mmu.walkingTranslations[0].cycleLeft).To(Equal(9))
			Expect(madeProgress).To(BeTrue())
		})

		It("should do nothing if page is being migrated", func() {
			page := vm.Page{
				PID:         1,
				VAddr:       0x1000,
				PAddr:       0x0,
				PageSize:    4096,
				Valid:       true,
				IsMigrating: true,
			}
			req := vm.TranslationReqBuilder{}.
				WithSendTime(10).
				WithDst(toTop).
				WithPID(1).
				WithVAddr(0x1020).
				WithGPUID(0).
				Build()
			walking := transaction{req: req, cycleLeft: 0}
			mmu.walkingTranslations = append(mmu.walkingTranslations, walking)
			mmu.numRequestsInFlight = 1

			pageTable.EXPECT().
				Find(ca.PID(1), uint64(0x1020)).
				Return(page, true)

			madeProgress := mmu.walkPageTable(11)

			Expect(madeProgress).To(BeFalse())
			Expect(mmu.numRequestsInFlight).To(Equal(1))
		})

		It("should send rsp to top if hit", func() {
			page := vm.Page{
				PID:      1,
				VAddr:    0x1000,
				PAddr:    0x0,
				PageSize: 4096,
				Valid:    true,
			}
			req := vm.TranslationReqBuilder{}.
				WithSendTime(10).
				WithDst(mmu.ToTop).
				WithPID(1).
				WithVAddr(0x1000).
				WithGPUID(0).
				Build()
			walking := transaction{req: req, cycleLeft: 0}
			mmu.walkingTranslations = append(mmu.walkingTranslations, walking)
			mmu.numRequestsInFlight = 1

			pageTable.EXPECT().
				Find(ca.PID(1), uint64(0x1000)).
				Return(page, true)
			topSender.EXPECT().CanSend(1).Return(true)
			topSender.EXPECT().
				Send(gomock.Any()).
				Do(func(rsp *vm.TranslationRsp) {
					Expect(rsp.Page).To(Equal(page))
				})

			madeProgress := mmu.walkPageTable(11)

			Expect(madeProgress).To(BeTrue())
			Expect(mmu.numRequestsInFlight).To(Equal(0))
			Expect(mmu.walkingTranslations).To(HaveLen(0))
		})

		It("should stall if cannot send to top", func() {
			page := vm.Page{
				PID:      1,
				VAddr:    0x1000,
				PAddr:    0x0,
				PageSize: 4096,
				Valid:    true,
			}
			req := vm.TranslationReqBuilder{}.
				WithSendTime(10).
				WithDst(mmu.ToTop).
				WithPID(1).
				WithVAddr(0x1000).
				WithGPUID(0).
				Build()
			walking := transaction{req: req, cycleLeft: 0}
			mmu.walkingTranslations =
				append(mmu.walkingTranslations, walking)
			mmu.numRequestsInFlight = 1

			pageTable.EXPECT().
				Find(ca.PID(1), uint64(0x1000)).
				Return(page, true)
			topSender.EXPECT().CanSend(1).Return(false)

			madeProgress := mmu.walkPageTable(11)

			Expect(madeProgress).To(BeFalse())
			Expect(mmu.numRequestsInFlight).To(Equal(1))
		})
	})

	Context("migration", func() {
		var (
			page    vm.Page
			req     *vm.TranslationReq
			walking transaction
		)

		BeforeEach(func() {
			page = vm.Page{
				PID:      1,
				VAddr:    0x1000,
				PAddr:    0x0,
				PageSize: 4096,
				Valid:    true,
				GPUID:    2,
				Unified:  true,
			}
			pageTable.EXPECT().
				Find(ca.PID(1), uint64(0x1000)).
				Return(page, true).
				AnyTimes()
			req = vm.TranslationReqBuilder{}.
				WithSendTime(10).
				WithDst(mmu.ToTop).
				WithPID(1).
				WithVAddr(0x1000).
				WithGPUID(0).
				Build()
			walking = transaction{req: req, cycleLeft: 0}
			mmu.walkingTranslations = append(
				mmu.walkingTranslations, walking)
		})

		It("should stall if the MMU is doing another migration", func() {
			mmu.isDoingMigration = true

			madeProgress := mmu.walkPageTable(11)

			Expect(madeProgress).To(BeFalse())
		})

		It("should stall if send failed", func() {
			migrationPort.EXPECT().
				Send(gomock.Any()).
				Return(akita.NewSendError())

			madeProgress := mmu.walkPageTable(11)

			Expect(madeProgress).To(BeFalse())
			Expect(mmu.walkingTranslations).To(HaveLen(1))
		})

		It("should send to migrator if need migration", func() {
			migrationPort.EXPECT().Send(gomock.Any())

			updatedPage := page
			updatedPage.IsMigrating = true
			pageTable.EXPECT().Update(updatedPage)

			madeProgress := mmu.walkPageTable(11)

			Expect(madeProgress).To(BeTrue())
			Expect(mmu.walkingTranslations).To(HaveLen(0))
			Expect(mmu.currentOnDemandMigration.migration).NotTo(BeNil())
			Expect(mmu.isDoingMigration).To(BeTrue())
		})
	})

	Context("should return migrated page information", func() {
		var (
			page          vm.Page
			req           *vm.TranslationReq
			migrating     transaction
			migrationDone *vm.PageMigrationRspFromDriver
		)

		BeforeEach(func() {
			page = vm.Page{
				PID:         1,
				VAddr:       0x1000,
				PAddr:       0x0,
				PageSize:    4096,
				Valid:       true,
				GPUID:       1,
				Unified:     true,
				IsMigrating: true,
			}
			pageTable.EXPECT().
				Find(ca.PID(1), uint64(0x1000)).
				Return(page, true).
				AnyTimes()
			req = vm.TranslationReqBuilder{}.
				WithSendTime(10).
				WithDst(mmu.ToTop).
				WithPID(1).
				WithVAddr(0x1000).
				WithGPUID(0).
				Build()
			migrating = transaction{req: req, cycleLeft: 0}
			mmu.currentOnDemandMigration = migrating
			mmu.numRequestsInFlight = 1
			migrationDone = vm.NewPageMigrationRspFromDriver(0, nil, nil)
		})

		It("should do nothing if no respond", func() {
			migrationPort.EXPECT().Peek().Return(nil)

			madeProgress := mmu.processMigrationReturn(10)

			Expect(madeProgress).To(BeFalse())
		})

		It("should stall if send to top failed", func() {
			migrationPort.EXPECT().Peek().Return(migrationDone)
			topSender.EXPECT().CanSend(1).Return(false)

			madeProgress := mmu.processMigrationReturn(10)

			Expect(madeProgress).To(BeFalse())
			Expect(mmu.numRequestsInFlight).To(Equal(1))
			Expect(mmu.isDoingMigration).To(BeFalse())
		})

		It("should send rsp to top", func() {
			migrationPort.EXPECT().Peek().Return(migrationDone)
			topSender.EXPECT().CanSend(1).Return(true)
			topSender.EXPECT().Send(gomock.Any()).
				Do(func(rsp *vm.TranslationRsp) {
					Expect(rsp.Page).To(Equal(page))
				})
			migrationPort.EXPECT().Retrieve(gomock.Any())

			updatedPage := page
			updatedPage.IsPinned = true
			updatedPage.IsMigrating = false
			pageTable.EXPECT().Update(updatedPage)

			madeProgress := mmu.processMigrationReturn(10)

			Expect(madeProgress).To(BeTrue())
			Expect(mmu.numRequestsInFlight).To(Equal(0))
			Expect(mmu.isDoingMigration).To(BeFalse())
		})

	})
})

var _ = Describe("MMU Integration", func() {
	var (
		mockCtrl   *gomock.Controller
		engine     akita.Engine
		mmu        *MMUImpl
		agent      *MockPort
		connection akita.Connection
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		engine = akita.NewSerialEngine()

		builder := MakeBuilder().WithEngine(engine)
		mmu = builder.Build("mmu")
		agent = NewMockPort(mockCtrl)
		connection = akita.NewDirectConnection("conn", engine, 1*akita.GHz)

		agent.EXPECT().SetConnection(connection)
		connection.PlugIn(agent, 10)
		connection.PlugIn(mmu.ToTop, 10)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should lookup", func() {
		page := vm.Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x2000,
			PageSize: 4096,
			Valid:    true,
			GPUID:    1,
		}
		mmu.pageTable.Insert(page)

		req := vm.TranslationReqBuilder{}.
			WithSendTime(10).
			WithSrc(agent).
			WithDst(mmu.ToTop).
			WithPID(1).
			WithVAddr(0x1000).
			WithGPUID(0).
			Build()
		req.RecvTime = 10
		mmu.ToTop.Recv(req)

		agent.EXPECT().Recv(gomock.Any()).
			Do(func(rsp *vm.TranslationRsp) {
				Expect(rsp.Page).To(Equal(page))
				Expect(rsp.RespondTo).To(Equal(req.ID))
			})

		engine.Run()
	})
})
