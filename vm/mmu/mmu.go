package mmu

import (
	"log"
	"reflect"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem/vm"
	"gitlab.com/akita/util/akitaext"
	"gitlab.com/akita/util/ca"
)

type transaction struct {
	req       *vm.TranslationReq
	page      vm.Page
	cycleLeft int
	migration *vm.PageMigrationReqToDriver
}

// MMUImpl is the default mmu implementation. It is also an akita Component.
type MMUImpl struct {
	akita.TickingComponent

	ToTop                    akita.Port
	MigrationPort            akita.Port
	MigrationServiceProvider akita.Port
	topSender                akitaext.BufferedSender

	pageTable           vm.PageTable
	latency             int
	maxRequestsInFlight int
	numRequestsInFlight int

	walkingTranslations      []transaction
	currentOnDemandMigration transaction
	isDoingMigration         bool

	toRemoveFromPTW    []int
	PageAccesedByGPUID map[uint64][]uint64
}

// Tick defines how the MMU update state each cycle
func (mmu *MMUImpl) Tick(now akita.VTimeInSec) bool {
	madeProgress := false

	madeProgress = mmu.topSender.Tick(now) || madeProgress
	madeProgress = mmu.walkPageTable(now) || madeProgress
	madeProgress = mmu.processMigrationReturn(now) || madeProgress
	madeProgress = mmu.parseFromTop(now) || madeProgress

	return madeProgress
}

func (mmu *MMUImpl) trace(now akita.VTimeInSec, what string) {
	ctx := akita.HookCtx{
		Domain: mmu,
		Now:    now,
		Item:   what,
	}

	mmu.InvokeHook(ctx)
}

func (mmu *MMUImpl) walkPageTable(now akita.VTimeInSec) bool {
	madeProgress := false
	for i := 0; i < len(mmu.walkingTranslations); i++ {
		if mmu.walkingTranslations[i].cycleLeft > 0 {
			mmu.walkingTranslations[i].cycleLeft--
			madeProgress = true
			continue
		}

		madeProgress = mmu.finalizePageWalk(now, i) || madeProgress
	}

	tmp := mmu.walkingTranslations[:0]

	for i := 0; i < len(mmu.walkingTranslations); i++ {
		if !mmu.toRemove(i) {
			tmp = append(tmp, mmu.walkingTranslations[i])
		}
	}

	mmu.walkingTranslations = tmp

	mmu.toRemoveFromPTW = nil

	return madeProgress
}

func (mmu *MMUImpl) finalizePageWalk(
	now akita.VTimeInSec,
	walkingIndex int,
) bool {
	req := mmu.walkingTranslations[walkingIndex].req
	page, found := mmu.pageTable.Find(req.PID, req.VAddr)

	if !found {
		panic("page not found")
	}

	if page.IsMigrating {
		return false
	}

	mmu.walkingTranslations[walkingIndex].page = page

	if mmu.pageNeedMigrate(mmu.walkingTranslations[walkingIndex]) {
		return mmu.doOnDemandMigration(now, walkingIndex)
	}

	return mmu.doPageWalkHit(now, walkingIndex)
}

func (mmu MMUImpl) pageNeedMigrate(walking transaction) bool {
	if walking.req.GPUID == walking.page.GPUID {
		return false
	}

	if !walking.page.Unified {
		return false
	}

	if walking.page.IsPinned {
		return false
	}

	return true
}

func (mmu *MMUImpl) doPageWalkHit(
	now akita.VTimeInSec,
	walkingIndex int,

) bool {
	if !mmu.topSender.CanSend(1) {
		return false
	}
	walking := mmu.walkingTranslations[walkingIndex]

	rsp := vm.TranslationRspBuilder{}.
		WithSendTime(now).
		WithSrc(mmu.ToTop).
		WithDst(walking.req.Src).
		WithRspTo(walking.req.ID).
		WithPage(walking.page).
		Build()

	mmu.topSender.Send(rsp)
	mmu.numRequestsInFlight--
	mmu.toRemoveFromPTW = append(mmu.toRemoveFromPTW, walkingIndex)
	return true
}

func (mmu *MMUImpl) doOnDemandMigration(
	now akita.VTimeInSec,
	walkingIndex int,
) bool {
	if mmu.isDoingMigration {
		return false
	}

	walking := mmu.walkingTranslations[walkingIndex]
	page := walking.page
	migrationInfo := new(vm.PageMigrationInfo)
	migrationInfo.GpuReqToVAddrMap = make(map[uint64][]uint64)
	migrationInfo.GpuReqToVAddrMap[walking.req.GPUID] =
		append(migrationInfo.GpuReqToVAddrMap[walking.req.GPUID],
			walking.req.VAddr)

	mmu.PageAccesedByGPUID[page.VAddr] =
		append(mmu.PageAccesedByGPUID[page.VAddr], page.GPUID)

	req := vm.NewPageMigrationReqToDriver(
		now, mmu.MigrationPort, mmu.MigrationServiceProvider)
	req.PID = page.PID
	req.PageSize = page.PageSize
	req.CurrPageHostGPU = page.GPUID
	req.MigrationInfo = migrationInfo
	req.CurrAccessingGPUs = unique(mmu.PageAccesedByGPUID[page.VAddr])
	req.RespondToTop = true

	err := mmu.MigrationPort.Send(req)
	if err != nil {
		return false
	}

	walking.page.IsMigrating = true
	mmu.pageTable.Update(walking.page)
	walking.migration = req
	mmu.isDoingMigration = true
	mmu.currentOnDemandMigration = walking
	mmu.toRemoveFromPTW = append(mmu.toRemoveFromPTW, walkingIndex)
	return true
}

func (mmu *MMUImpl) processMigrationReturn(now akita.VTimeInSec) bool {
	item := mmu.MigrationPort.Peek()
	if item == nil {
		return false
	}

	if !mmu.topSender.CanSend(1) {
		return false
	}

	req := mmu.currentOnDemandMigration.req
	page, found := mmu.pageTable.Find(req.PID, req.VAddr)
	if !found {
		panic("page not found")
	}

	rsp := vm.TranslationRspBuilder{}.
		WithSendTime(now).
		WithSrc(mmu.ToTop).
		WithDst(req.Src).
		WithRspTo(req.ID).
		WithPage(page).
		Build()
	mmu.topSender.Send(rsp)

	page.IsPinned = true
	page.IsMigrating = false
	mmu.pageTable.Update(page)
	// mmu.currentOnDemandMigration.page = page
	mmu.isDoingMigration = false
	mmu.numRequestsInFlight--
	mmu.MigrationPort.Retrieve(now)

	return true
}

func (mmu *MMUImpl) parseFromTop(now akita.VTimeInSec) bool {
	if mmu.numRequestsInFlight >= mmu.maxRequestsInFlight {
		return false
	}

	req := mmu.ToTop.Retrieve(now)
	if req == nil {
		return false
	}

	switch req := req.(type) {
	case *vm.TranslationReq:
		mmu.startWalking(req)
	default:
		log.Panicf("MMU canot handle request of type %s", reflect.TypeOf(req))
	}

	mmu.numRequestsInFlight++
	return true
}

func (mmu *MMUImpl) startWalking(req *vm.TranslationReq) {
	translationInPipeline := transaction{
		req:       req,
		cycleLeft: mmu.latency,
	}

	mmu.walkingTranslations = append(mmu.walkingTranslations, translationInPipeline)
}

func (mmu *MMUImpl) MarkPageAsMigrating(vAddr uint64, pid ca.PID) {

	// page := mmu.pageTabl

	// table := mmu.getOrCreatePageTable(pid)

	// page := table.FindPage(vAddr)
	// if page == nil {
	// 	log.Panicf("Trying to invalidate a page that does not exist")
	// }
	// page.IsMigrating = true
}

func (mmu *MMUImpl) toRemove(index int) bool {
	for i := 0; i < len(mmu.toRemoveFromPTW); i++ {
		remove := mmu.toRemoveFromPTW[i]
		if remove == index {
			return true
		}
	}
	return false
}

func unique(intSlice []uint64) []uint64 {
	keys := make(map[int]bool)
	list := []uint64{}
	for _, entry := range intSlice {
		if _, value := keys[int(entry)]; !value {
			keys[int(entry)] = true
			list = append(list, entry)
		}
	}
	return list
}
