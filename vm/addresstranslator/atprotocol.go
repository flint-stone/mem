package addresstranslator

import (
	"gitlab.com/akita/akita"
)

// An AddressTranslatorFlushReq asks the AT to clear its ports and any buffers.
// It will also not block all incoming and outgoing ports
type AddressTranslatorFlushReq struct {
	akita.MsgMeta
}

// Meta returns the meta data associated with the message.
func (r *AddressTranslatorFlushReq) Meta() *akita.MsgMeta {
	return &r.MsgMeta
}

// AddressTranslatorFlushReqBuilder can build AT flush requests
type AddressTranslatorFlushReqBuilder struct {
	sendTime akita.VTimeInSec
	src, dst akita.Port
}

// WithSendTime sets the send time of the request to build.:w
func (b AddressTranslatorFlushReqBuilder) WithSendTime(
	t akita.VTimeInSec,
) AddressTranslatorFlushReqBuilder {
	b.sendTime = t
	return b
}

// WithSrc sets the source of the request to build.
func (b AddressTranslatorFlushReqBuilder) WithSrc(src akita.Port) AddressTranslatorFlushReqBuilder {
	b.src = src
	return b
}

// WithDst sets the destination of the request to build.
func (b AddressTranslatorFlushReqBuilder) WithDst(dst akita.Port) AddressTranslatorFlushReqBuilder {
	b.dst = dst
	return b
}

// Build creats a new AddressTranslatorFlushReq
func (b AddressTranslatorFlushReqBuilder) Build() *AddressTranslatorFlushReq {
	r := &AddressTranslatorFlushReq{}
	r.ID = akita.GetIDGenerator().Generate()
	r.Src = b.src
	r.Dst = b.dst
	r.SendTime = b.sendTime

	return r
}

// A AddressTranslatorFlushRsp is a response from AT indicating flush is complete
type AddressTranslatorFlushRsp struct {
	akita.MsgMeta
}

// Meta returns the meta data associated with the message.
func (r *AddressTranslatorFlushRsp) Meta() *akita.MsgMeta {
	return &r.MsgMeta
}

// AddressTranslatorFlushRspBuilder can build AT flush rsp
type AddressTranslatorFlushRspBuilder struct {
	sendTime akita.VTimeInSec
	src, dst akita.Port
}

// WithSendTime sets the send time of the request to build.:w
func (b AddressTranslatorFlushRspBuilder) WithSendTime(
	t akita.VTimeInSec,
) AddressTranslatorFlushRspBuilder {
	b.sendTime = t
	return b
}

// WithSrc sets the source of the request to build.
func (b AddressTranslatorFlushRspBuilder) WithSrc(src akita.Port) AddressTranslatorFlushRspBuilder {
	b.src = src
	return b
}

// WithDst sets the destination of the request to build.
func (b AddressTranslatorFlushRspBuilder) WithDst(dst akita.Port) AddressTranslatorFlushRspBuilder {
	b.dst = dst
	return b
}

// Build creats a new AddressTranslatorFlushRsps
func (b AddressTranslatorFlushRspBuilder) Build() *AddressTranslatorFlushRsp {
	r := &AddressTranslatorFlushRsp{}
	r.ID = akita.GetIDGenerator().Generate()
	r.Src = b.src
	r.Dst = b.dst
	r.SendTime = b.sendTime

	return r
}

// A AddressTranslatorRestartReq is a request to AT to start accepting requests and resume operations
type AddressTranslatorRestartReq struct {
	akita.MsgMeta
}

// Meta returns the meta data associated with the message.
func (r *AddressTranslatorRestartReq) Meta() *akita.MsgMeta {
	return &r.MsgMeta
}

// AddressTranslatorFlushRspBuilder can build AT flush rsp
type AddressTranslatorRestartReqBuilder struct {
	sendTime akita.VTimeInSec
	src, dst akita.Port
}

// WithSendTime sets the send time of the request to build.:w
func (b AddressTranslatorRestartReqBuilder) WithSendTime(
	t akita.VTimeInSec,
) AddressTranslatorRestartReqBuilder {
	b.sendTime = t
	return b
}

// WithSrc sets the source of the request to build.
func (b AddressTranslatorRestartReqBuilder) WithSrc(src akita.Port) AddressTranslatorRestartReqBuilder {
	b.src = src
	return b
}

// WithDst sets the destination of the request to build.
func (b AddressTranslatorRestartReqBuilder) WithDst(dst akita.Port) AddressTranslatorRestartReqBuilder {
	b.dst = dst
	return b
}

// Build creats a new AddressTranslatorRestartReq
func (b AddressTranslatorRestartReqBuilder) Build() *AddressTranslatorRestartReq {
	r := &AddressTranslatorRestartReq{}
	r.ID = akita.GetIDGenerator().Generate()
	r.Src = b.src
	r.Dst = b.dst
	r.SendTime = b.sendTime

	return r
}

// A AddressTranslatorRestartRsp is a response from AT indicating it has resumed working
type AddressTranslatorRestartRsp struct {
	akita.MsgMeta
}

// Meta returns the meta data associated with the message.
func (r *AddressTranslatorRestartRsp) Meta() *akita.MsgMeta {
	return &r.MsgMeta
}

// AddressTranslatorRestartRspBuilder can build AT flush rsp
type AddressTranslatorRestartRspBuilder struct {
	sendTime akita.VTimeInSec
	src, dst akita.Port
}

// WithSendTime sets the send time of the request to build.:w
func (b AddressTranslatorRestartRspBuilder) WithSendTime(
	t akita.VTimeInSec,
) AddressTranslatorRestartRspBuilder {
	b.sendTime = t
	return b
}

// WithSrc sets the source of the request to build.
func (b AddressTranslatorRestartRspBuilder) WithSrc(src akita.Port) AddressTranslatorRestartRspBuilder {
	b.src = src
	return b
}

// WithDst sets the destination of the request to build.
func (b AddressTranslatorRestartRspBuilder) WithDst(dst akita.Port) AddressTranslatorRestartRspBuilder {
	b.dst = dst
	return b
}

// Build creates a new AddressTranslatorRestartRsp
func (b AddressTranslatorRestartRspBuilder) Build() *AddressTranslatorRestartRsp {
	r := &AddressTranslatorRestartRsp{}
	r.ID = akita.GetIDGenerator().Generate()
	r.Src = b.src
	r.Dst = b.dst
	r.SendTime = b.sendTime

	return r
}
